export default [
  {
    Id: 'FSI',
    Name: 'Fakulta strojního inženýrství',
    Color: '#5F5754',
    Rest: 'FSI'
  },
  {
    Id: 'FZP',
    Name: 'Fakulta životního prostředí',
    Color: '#59BF38',
    Rest: 'FŽP'
  },
  {
    Id: 'FUD',
    Name: 'Fakulta umění a designu',
    Color: '#FFC001',
    Rest: 'FUU'
  },
  {
    Id: 'FF',
    Name: 'Filozofická fakulta',
    Color: '#F56F00',
    Rest: 'FF',
  },
  {
    Id: 'FZS',
    Name: 'Fakulta zdravotnických studií',
    Color: '#D90009',
    Rest: 'FZS',
  },
  {
    Id: 'PRF',
    Name: 'Přírodovědecká fakulta',
    Color: '#007F88',
    Rest: 'PRF',
  },
  {
    Id: 'PF',
    Name: 'Pedagogická fakulta',
    Color: '#F12D99',
    Rest: 'PF',
  },
  {
    Id: 'FSE',
    Name: 'Fakulta sociálně ekonomická',
    Color: '#0998DA',
    Rest: 'FSE',
  }
]