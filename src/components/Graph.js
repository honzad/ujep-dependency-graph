import React, { Component } from 'react';
import Panzoom from 'panzoom';
import SVG from 'svg.js';
import EasyStar from 'easystarjs';

import { Spin, Icon  } from 'antd';

import './Graph.css';

const DirectionsEnum = Object.freeze({"UP":1, "RIGHT":2, "DOWN":3, "LEFT":4})

const itemSize = {
  x: 300,
  y: 300,
};
const itemGap = {
  x: 300,
  y: 300,
};

const dividerNumber = 2;


export default class Graph extends Component {

  drawer = undefined;


  constructor(props){
    super(props);

    this.state = {
      isLoading: true,
    }
  }

  componentDidMount() {
    const { courseId, includeNonCompulsory } = this.props

    var scene = document.getElementById('scene')
    Panzoom(scene, {
      autocenter: true,
      bounds: true
    })

    this.drawer = SVG('scene').size('100%', '100%');

    if(this.state.isLoading) {
      this.getSubjectData(courseId, includeNonCompulsory)
      .then(d => this.createDataSet(d))
      .then(d => this.getGrid(d))
      .then(d => this.createGraph(d))
      .then(() => {
        this.setState({
          isLoading: false,
        })
      })
      .catch(e => console.log(e))
    }
  }

  formatParams = (params) => {
    return "?" + Object
      .keys(params)
      .map(function(key){
        return key+"="+encodeURIComponent(params[key])
      })
      .join("&")
  }

  createDataSet = (data) => {
    return new Promise((res) => {
      let idList = [],
      missingIdList = [],
      dataSet = {},
      orderedDataSet = [],
      EdgeList = [];
      
      data.forEach((subject) => {
        idList.push(subject.SubjectId);
        if(!(subject.SubjectRecommendedYear in dataSet)){
          dataSet[subject.SubjectRecommendedYear] = {};
          if(!(subject.SubjectRecommendedSemester in dataSet[subject.SubjectRecommendedYear])){
            dataSet[subject.SubjectRecommendedYear][subject.SubjectRecommendedSemester] = [];
          }
        }else{
          if(!(subject.SubjectRecommendedSemester in dataSet[subject.SubjectRecommendedYear])){
            dataSet[subject.SubjectRecommendedYear][subject.SubjectRecommendedSemester] = [];
          }
        }
        dataSet[subject.SubjectRecommendedYear][subject.SubjectRecommendedSemester].push(subject);
      })

      let lastId = 0;
      for(var key in dataSet){
        let newId = parseInt(key);
        while(newId < lastId){
          newId++;
        }
        for(var cKey in dataSet[key]){
          orderedDataSet[newId] = dataSet[key][cKey];
          newId++;
        }
        lastId = newId;
      }

      data.forEach((subject) => {
        if(subject.SubjectConditionalSubjects.length > 0){
          subject.SubjectConditionalSubjects.forEach((cSub)=>{
            if(idList.includes(cSub)){
              EdgeList.push({
                source: cSub, 
                target: subject.SubjectId
              });
            }else{
              missingIdList.push(cSub);
            }
          })
        }
      });

      res({
        Nodes: orderedDataSet,
        Edges: EdgeList
      })

    })
  }

  getAngleFromVector = (x,y) => {
    let rad = Math.atan2(y, x);   //radians
    let degrees = 180*rad/Math.PI;  //degrees
    let angle = (360+Math.round(degrees))%360; //round number, avoid decimal fragments
    //

    switch(this.getDirectionFromDegree(angle)){
      case 1:
      return this.invertAngle(angle);

      case 2:
      return angle;

      case 3:
      return this.invertAngle(angle);

      case 4:
      return angle;

      default:
      console.log('invalid direction');
      return angle;
    }
  }

  invertAngle = (angle) => {
    if(angle + 180 >= 360){
      return angle - 180;
    }else{
      return angle + 180;
    }
  }

  getDirectionFromDegree = (degree) => {
    if(this.numberBetween(degree, 0, 44)){
      return DirectionsEnum.RIGHT;
    }else if(this.numberBetween(degree, 315, 359)){
      return DirectionsEnum.RIGHT;
    }else if(this.numberBetween(degree, 45, 134)){
      return DirectionsEnum.UP;
    }else if(this.numberBetween(degree, 135, 224)){
      return DirectionsEnum.LEFT;
    }else if(this.numberBetween(degree, 225, 314)){
      return DirectionsEnum.DOWN;
    }
  }

  getGrid = ({ Nodes, Edges }) => {
    return new Promise((res) => {
      const micro = false
      const startGap = itemGap.x;

      let maxX = startGap + ((itemSize.x + itemGap.x) * Nodes.length), 
      maxY = 0;

      Nodes.forEach((v, colNum) => {
        if(maxY < startGap + ((itemSize.y + itemGap.y) * v.length)){
          maxY = startGap + ((itemSize.y + itemGap.y) * v.length);
        }

        let xPosNormal = startGap + ((itemSize.x + itemGap.x) * colNum);
        let xPosGrid = (xPosNormal / (itemSize.x / dividerNumber));
        let xPosGridMicro = (xPosNormal / itemSize.x);
        let width = itemSize.x;
        let widthGrid = (itemSize.x / (itemSize.x / dividerNumber));
        let widthGridMicro = (itemSize.x / itemSize.x);

        v.forEach((item, rowNum) => {
          let yPosNormal = startGap + (itemSize.y + itemGap.y) * rowNum;
          let yPosGrid = (yPosNormal / (itemSize.y / dividerNumber));
          let yPosGridMicro = (yPosNormal / itemSize.y);
          let height = itemSize.y;
          let heightGrid = (itemSize.y / (itemSize.y / dividerNumber));
          let heightGridMicro = (itemSize.y / itemSize.y);
          

          item.Transform = {
            x: {
              xPosNormal,
              xPosGrid,
              xPosGridMicro
            },
            y: {
              yPosNormal,
              yPosGrid,
              yPosGridMicro
            },
            width: {
              width,
              widthGrid,
              widthGridMicro
            },
            height: {
              height,
              heightGrid,
              heightGridMicro
            }
          }
        })
      })

      /*
      let Border = {
        width: maxX,
        widthGrid: maxX / (itemSize.x / dividerNumber),
        widthGridMicro: maxX / itemSize.x,
        height: maxY,
        heightGrid: maxY / (itemSize.y / dividerNumber),
        heightGridMicro: maxY / itemSize.y
      }
      */

      // Setting grid size [maxX, maxY]
      if(micro){
        maxX = maxX / itemSize.x;
        maxY = maxY / itemSize.y;
      }else{
        maxX = maxX / (itemSize.x / dividerNumber);
        maxY = maxY / (itemSize.y / dividerNumber);
      }

      let grid = this.createMatrix(maxX, maxY);

      Nodes.forEach((col, colNum) => {
        col.forEach((item, rowNum) => {
          const { xPosGrid } = item.Transform.x
          const { yPosGrid } = item.Transform.y
          const { widthGrid } = item.Transform.width
          const { heightGrid } = item.Transform.height

          for(var i = xPosGrid; i <= xPosGrid + widthGrid; i++){
            for( var j = yPosGrid; j <= yPosGrid + heightGrid; j++){
              grid[j][i] = 1;
            }
          }
        })
      })

      let PathList = [];

      var es = new EasyStar.js();
      es.setGrid(grid)
      es.setAcceptableTiles([0]);
      es.setIterationsPerCalculation(1000);

      var promises = Edges.map(({ source, target }) => {
        return new Promise((resolve) => {
          let sourceNode, targetNode;

          Promise.all([this.findInDataset(source, Nodes),this.findInDataset(target, Nodes)])
          .then((values)=>{
            sourceNode = values[0];
            targetNode = values[1];

            let directionVectorSource = this.getVectorFromPosition(
              {x:sourceNode.Transform.x.xPosGridMicro, y:sourceNode.Transform.y.yPosGridMicro},
              {x:targetNode.Transform.x.xPosGridMicro, y:targetNode.Transform.y.yPosGridMicro}
            )
    
            let directionVectorTarget = this.getVectorFromPosition(
              {x:targetNode.Transform.x.xPosGridMicro, y:targetNode.Transform.y.yPosGridMicro},
              {x:sourceNode.Transform.x.xPosGridMicro, y:sourceNode.Transform.y.yPosGridMicro}
            )
    
            let directionSource = this.getDirectionFromDegree(this.getAngleFromVector(directionVectorSource.x,directionVectorSource.y));
            let directionTarget = this.getDirectionFromDegree(this.getAngleFromVector(directionVectorTarget.x,directionVectorTarget.y));
    
            let sourcePoint = this.getStartPoint(directionSource, sourceNode), 
            targetPoint = this.getStartPoint(directionTarget, targetNode);


            grid[sourcePoint.y][sourcePoint.x] = 0;
            grid[targetPoint.y][targetPoint.x] = 0;

            es.calculate();
            es.findPath(sourcePoint.x, sourcePoint.y, targetPoint.x, targetPoint.y, (path) => {
              if(path !== null) {
                let pObj = {
                  source,
                  target,
                  path: []
                };
                path.forEach((p) => { // Normalize
                  const px = p.x * (itemSize.x / dividerNumber);
                  const py = p.y * (itemSize.y / dividerNumber);
                  pObj.path.push([px,py])
                })
                PathList.push(pObj);
              }else{
                console.log('invalid path', sourcePoint.x, sourcePoint.y, targetPoint.x, targetPoint.y);
              }
            });
          })
          .then(() => {
            setTimeout(() => {
              resolve()
            }, 1000);
          })
        })
      })

      Promise.all(promises).then(() => {
        res({
          Nodes,
          PathList
        });
      })
    })
  }

  getStartPoint = (dir, node) => {
    let point;

    switch(dir){
      case DirectionsEnum.UP:
        point = {
          x: node.Transform.x.xPosGrid + 1,
          y: node.Transform.y.yPosGrid,
        }
      break;
      case DirectionsEnum.RIGHT:
        point = {
          x: node.Transform.x.xPosGrid + 2,
          y: node.Transform.y.yPosGrid + 1,
        }
      break;
      case DirectionsEnum.DOWN:
        point = {
          x: node.Transform.x.xPosGrid + 1,
          y: node.Transform.y.yPosGrid + 2,
        }
      break;
      case DirectionsEnum.LEFT:
        point = {
          x: node.Transform.x.xPosGrid,
          y: node.Transform.y.yPosGrid + 1,
        }
      break;
      default:
        point = null;
      break;
    }

    return point;
  }

  findInDataset = (id, arr) => {
    return new Promise((res) => {
      arr.forEach((col) => {
        col.forEach((val) => {
          if(val.SubjectId === id){
            res(val)
          }
        })
      })
    })
  }

  createMatrix = (x, y) => {
    return (Array(y).fill(null)).map(()=>Array(x).fill(0))
  }

  numberBetween = (x, min , max) => {
    return ( x >= min && x <= max );
  }

  getVectorFromPosition = (first, second) => {
    return {
      x: second.x - first.x,
      y: second.y - first.y
    }
  }

  createGraph = ({ Nodes, PathList }) => {
    return new Promise((res) => {
      const { baseColor } = this.props



      Nodes.forEach((col) => {
        col.forEach((item) => {
          item.connectedPaths = [];
          PathList.forEach(({source, target, path}) => {
            if(item.SubjectId === source){
              item.connectedPaths.push(path);
            }else if (item.SubjectId === target){
              item.connectedPaths.push(path);
            }
          })
        })
      })

      let pathGroup = this.drawer.group();
      pathGroup.opacity(1.0);

      let selectedNode = null;

      const clickEvent = (node) => {
        if(selectedNode !== null){ // Node previously selected
          if(selectedNode.SubjectId === node.SubjectId){ // Turn off this node
            node.pathGroup.opacity(0.0);
            selectedNode.pathGroup.opacity(0.0);
            selectedNode = null;
            pathGroup.opacity(1.0);
          }else{ // Switch to other node
            selectedNode.pathGroup.opacity(0.0);
            node.pathGroup.opacity(1.0);
            selectedNode = node;
          }
        }else{ // No node set
          pathGroup.opacity(0.0);
          node.pathGroup.opacity(1.0);
          selectedNode = node;
        }
      }

      const createArrow = (group, preLastPath, lastPath) => {        
        let ve = this.getVectorFromPosition({x: preLastPath[0], y: preLastPath[1]}, {x: lastPath[0], y: lastPath[1]});
        let dir = this.getDirectionFromDegree(this.getAngleFromVector(ve.x,ve.y));
        
        let trianglePath = [];
        
        switch(dir){
          case 1:
          trianglePath.push(lastPath);
          trianglePath.push([lastPath[0] - 15, lastPath[1] + 15]);
          trianglePath.push([lastPath[0] + 15, lastPath[1] + 15]);
          group.polygon(trianglePath).stroke({ color: baseColor, width: 1 }).fill(baseColor);
          break;
          case 2:
          trianglePath.push(lastPath);
          trianglePath.push([lastPath[0] - 15, lastPath[1] - 15]);
          trianglePath.push([lastPath[0] - 15, lastPath[1] + 15]);
          group.polygon(trianglePath).stroke({ color: baseColor, width: 1 }).fill(baseColor);
          break;
          case 3:
          trianglePath.push(lastPath);
          trianglePath.push([lastPath[0] - 15, lastPath[1] - 15]);
          trianglePath.push([lastPath[0] + 15, lastPath[1] - 15]);
          group.polygon(trianglePath).stroke({ color: baseColor, width: 1 }).fill(baseColor);
          break;
          case 4:
          trianglePath.push(lastPath);
          trianglePath.push([lastPath[0] + 15, lastPath[1] - 15]);
          trianglePath.push([lastPath[0] + 15, lastPath[1] + 15]);
          group.polygon(trianglePath).stroke({ color: baseColor, width: 1 }).fill(baseColor);
          break;
          default:
          break;
        }
      }

      Nodes.forEach((col, colNum) => {
        this.drawer.text(colNum+'. Semestr.').move(150 + itemSize.x + (itemSize.x + itemGap.x) * colNum, 200).font({ fill: '#000', family: 'Inconsolata', anchor: 'middle', size: 32 });
        col.forEach((item) => {
          let connectedPathGroup = this.drawer.group();

          let group = this.drawer.group();
          group.rect(item.Transform.width.width,item.Transform.height.height).fill(baseColor).radius(5)
          group.text(item.SubjectName).move(item.Transform.width.width / 2,25).font({ fill: '#fff', family: 'Inconsolata', anchor: 'middle', weight: 'bold' })
          group.text(item.SubjectId).move(item.Transform.width.width / 2,40).font({ fill: '#fff', family: 'Inconsolata', anchor: 'middle' })
          group.text("Y: "+(item.SubjectRecommendedYear === -1 ? 'Any' :  item.SubjectRecommendedYear)+" S: "+item.SubjectRecommendedSemester).move( 10 ,item.Transform.height.height - 25).font({ fill: '#fff', family: 'Inconsolata', anchor: 'start' })
          group.text("C: "+item.SubjectCredits).move( item.Transform.width.width - 10 ,item.Transform.height.height - 25).font({ fill: '#fff', family: 'Inconsolata', anchor: 'end' })

          group.move(item.Transform.x.xPosNormal, item.Transform.y.yPosNormal);
          group.click(()=>{
            clickEvent(item);
          });

          item.connectedPaths.forEach((cp) => {
            connectedPathGroup.polyline(cp).stroke({ color: baseColor, width: 4, linecap: 'round', linejoin: 'round' }).fill('none');
            createArrow(connectedPathGroup, cp[cp.length - 2], cp[cp.length - 1]);
          })

          connectedPathGroup.opacity(0.0);
          item.pathGroup = connectedPathGroup;
        })
      })


      PathList.forEach(({ source, target, path }) => {
        pathGroup.polyline(path).stroke({ color: baseColor, width: 4, linecap: 'round', linejoin: 'round' }).fill('none');
        createArrow(pathGroup, path[path.length - 2], path[path.length - 1]);
      })

      res();
    })
  }

  getSubjectData = (CourseId, IncludeNonCompulsory, Year) => {
    return new Promise((res,rej)=>{
      let params = {
        Subject: CourseId,
      };

      if(IncludeNonCompulsory){
        params["Optional"] = "";
      }

      if(Year){
        params["Year"] = Year;
      }

      fetch('http://stag.honzad.cz/Courses/GetSubjectsByCourseId'+this.formatParams(params))
      .then(response => response.json())
      .then(json => res(json))
      .catch(e => rej(e))
    });
  }

  render() {
    const { baseColor } = this.props

    const antIcon = <Icon type="sync" style={{ fontSize: 30, color: baseColor }} spin />;

    return(
      <div style={{width: '100%', height: '100%'}}>
        {
          (this.state.isLoading ? (
            <Spin indicator={antIcon} style={{position: 'absolute', top: '50%', right: 0, bottom: 0, left: 0, margin: 'auto'}} />
          ):(null))
        }
        <svg style={{width: '100%', height: '100%'}}>
          <g id="scene">

          </g>
        </svg>
      </div>
    )
  } 
}