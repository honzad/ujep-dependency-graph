import React, { Component } from 'react';
import posed from "react-pose";
import styled from "styled-components";
import { tween } from "popmotion";
import { SimpleSelect } from 'react-selectize'
import { Button, Checkbox } from 'antd'

import './Menu.css';
import facultyList from '../constants/facultyList';

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: 150px;
  position: relative;
`;

const Square = posed.div({
  fullscreen: {
    width: 75,
    height: 150,
    transition: tween
  },
  idle: {
    width: 75,
    height: 75,
    transition: tween
  }
});

const StyledSquare = styled(Square)`
  width: 75;
  height: 75;
`;

export default class Menu extends Component {
  constructor(props){
    super(props);

    this.state = {
      buttons: new Array(facultyList.length).fill(false),
      selected: false,
      left: false,
      titleText: 'Zvolte fakultu.',
      titleColor: '#000',
      selectedId: 'Undefined',
      selectedCourse: null,
      margin: 0,
      isLoading: false,
      courses: [],
      includeNonCompulsory: false,
    }
  }

  handleButtonClick = (info) => {
    if(this.state.isLoading){return;}

    const { buttons } = this.state;

    const lastState = buttons.slice()[info.Pos];
    const currentState = (lastState ? false : true);

    let a = this.state.buttons.fill(false);
    a[info.Pos] = (lastState ? false : true);

    let titleColor = (currentState ? info.Color : '#000'), 
    titleText = (currentState ? info.Name : 'Zvolte fakultu.'), 
    newSelectedId = (currentState ? info.Rest : 'Undefined'), 
    left = (info.Pos < 4),
    margin = (info.Pos < 4 ? (((info.Pos + 1) * 75) + 10) : ((facultyList.length - info.Pos) * 75) +10);

    if(newSelectedId !== 'Undefined'){
      this.setState({
        isLoading: true
      }, () => {
        this.getCoursesData(newSelectedId)
        .then((r) => {
          const dataArray = r.map((v)=>{
            return {label: v.CourseName, value: v.CourseId}
          })

          this.setState({
            buttons: a,
            titleText,
            titleColor,
            left,
            margin,
            selectedId: newSelectedId,
            selected: currentState,
            courses: dataArray,
            selectedCourse: null,
            isLoading: false
          }, () => {
            this.refs.selector.highlightFirstSelectableOption();
          })
        })
      })
    }else{
      this.setState({
        buttons: a,
        titleText,
        titleColor,
        left,
        margin,
        selectedId: newSelectedId,
        selected: currentState,
        courses: [],
        selectedCourse: null,
      })
    }
  }

  handleSubmitButtonClick = () => {
    const { selectedCourse, includeNonCompulsory, titleColor } = this.state;
    const { onSubmit } = this.props

    onSubmit({
      id: selectedCourse.value,
      include: includeNonCompulsory,
      color: titleColor,
    });
  }

  getCoursesData = (Faculty, Year, Eng) => {
    return new Promise((res,rej)=>{
      let params = {};
      params["Faculty"] = Faculty;
      params["Mini"] = "";

      if(Year){
        params["Year"] = Year;
      }

      if(Eng){
        params["Eng"] = "";
      }

      fetch('http://stag.honzad.cz/Courses/GetByFaculty'+this.formatParams(params))
      .then(response => response.json())
      .then(json => res(json))
      .catch(e => rej(e))
    });
  }

  formatParams = (params) => {
    return "?" + Object
      .keys(params)
      .map(function(key){
        return key+"="+encodeURIComponent(params[key])
      })
      .join("&")
  }

  render() {
    let style = (!this.state.selected  ? {} : (this.state.left ? { marginLeft: this.state.margin } : { marginRight: this.state.margin }))
    style.marginTop = 22;
    style.color = this.state.titleColor;
    style.fontSize = 20;
    style.textAlign = 'center';
    style.alignSelf = (this.state.selected ? (this.state.left ? 'flex-start' : 'flex-end') : 'center')

    return(
      <div className="menuContainer">
        <ButtonContainer>
          {
            facultyList.map((x, i) => {
              x.Pos = i;
              return this.getButton((i === 0), (i === facultyList.length - 1), x);
            })
          }
          <div 
            style={{
              width: '100%', height: 75, position: 'absolute', bottom: 0, zIndex: 1, display: 'flex', flexDirection: 'column'
            }}
          >
            <div style={style}>
              {this.state.titleText}
            </div>
          </div>
        </ButtonContainer>
        <div style={{width: '100%', height: 50, alignContent: 'center', alignItems: 'center', display: 'flex', flexDirection: 'column', marginTop: 25}}>
          <SimpleSelect
            ref="selector"
            style={{alignSelf: 'center'}}
            options={this.state.courses}
            disabled={(this.state.isLoading || this.state.selectedId === 'Undefined')}
            onValueChange={(val) => {
              this.setState({
                selectedCourse: val
              })
            }}
            theme="material"
            placeholder="Zvolte obor"
            transitionEnter
            transitionLeave
          />
        </div>
        <div style={{width: '100%', height: 100, alignContent: 'center', alignItems: 'center', display: 'flex', flexDirection: 'column', marginTop: 25}}>
          <Button
          style={{backgroundColor: (this.state.selectedId !== 'Undefined' ? this.state.titleColor : 'white'), color: (this.state.selectedId !== 'Undefined' ? 'white' : 'black'), width: 300, height: 40}}
          disabled={this.state.isLoading || (this.state.selectedCourse !== null ? false : true)}
          onClick={() => {
            this.handleSubmitButtonClick();
          }}
          >
            {'Vytvořit graf'}
          </Button>
          <Checkbox
            style={{ marginTop: 25}}
            onChange={(e)=>{
              this.setState({
                includeNonCompulsory: e.target.checked
              })
            }}
          >
            {'Zobrazit nepovinné předměty'}
          </Checkbox>
        </div>

      </div>
    )
  }

  getButton = (first, last, info) => {
    return (
      <StyledSquare 
        pose={this.state.buttons[info.Pos] ? "fullscreen" : "idle"}
        backgroundColor={info.Color}
        key={info.Id}
        style={{
          backgroundColor: info.Color, borderTopLeftRadius: (first ? 5 : 0), borderTopRightRadius: (last ? 5 : 0), display: 'flex', flexDirection:'column-reverse', textAlign: 'center', zIndex: 2
        }} 
        onClick={() => this.handleButtonClick(info)}
      >
        <div style={{paddingBottom: 5, color: 'white', fontSize: 10}}>
          {info.Id}
        </div>
      </StyledSquare>
    )
  }
}