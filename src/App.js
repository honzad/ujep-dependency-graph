import React, { Component } from 'react';
import './App.css';

import Menu from './components/Menu';
import Graph from './components/Graph';

class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      switched: false,
      courseId: null,
      includeNonCompulsory: false,
      color: '',
    }
  }

  onCourseSubmited = ({ id, include, color }) => {
    this.setState({
      switched: true,
      courseId: id,
      includeNonCompulsory: include,
      color,
    }, () => {
      console.log('Selected courseId:',id,'include non compulsory?: ',include);
    })
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          {
            (this.state.switched ? (<Graph courseId={this.state.courseId} includeNonCompulsory={this.state.includeNonCompulsory} baseColor={this.state.color} />) : <Menu onSubmit={this.onCourseSubmited} />)
          }
        </div>
      </div>
    );
  }
}

export default App;
