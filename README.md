# UJEP Dependancy

Projekt se záměrem zobrazit relace mezi předměty.

[Live projekt zde](http://udep.honzad.cz/)

## Development server

Run `yarn start` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Build

Run `yarn build` to build the project. The build artifacts will be stored in the `build/` directory.

## Running unit tests

Run `yarn test` to execute the unit tests.
